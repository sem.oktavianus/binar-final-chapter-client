const itemStep = document.querySelectorAll(".accordion-item");
const steps = document.querySelector(".steps");

itemStep.forEach((step) => {
  step.addEventListener("click", () => {
    const lists = steps.children;
    for (list of lists) {
      list.classList.remove("active-steps");
    }
    step.classList.add("active-steps");
  });
});

const inputs = document.querySelectorAll("input");

inputs.forEach(input=>{
  input.addEventListener("focus", function () {
    if (input.value !== "") {
      input.style.backgroundColor = "#000";
    }
  });
})




