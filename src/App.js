import React from 'react'
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from 'react-router-dom'
import './App.scss'
import Header from './pages/header/header.component'
import Hero from './components/hero/hero.component'
import Thegame from './components/thegame/thegame.component'
import Features from './components/features/features.component'
import Topscore from './components/topscore/topscore.component'
import Sysreq from './components/sysreq/sysreq.component'
import Subscribe from './components/subscribe/subscribe.component'
import Footer from './components/footer/footer.component'
import Login from './pages/login/login.component'
import Register from './pages/register/register.component'
import Gamelist from './pages/gamelist/gamelist.component'
import GameSuite from './pages/gamesuit/gamesuite.component'
import UserList from './pages/users/users.component'
import UserDetail from './pages/users/userById.component'
import EditUser from './pages/users/editUsers.component'
import GameDetail from './pages/gamedetail/Gamedetail.component'
import Tetris from './pages/tetris/tetris'
import Snake from './pages/snake/snake'
import Media from '../src/pages/media/media'

function App() {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <>
        <Route path="/" element={<Header />}>
          <Route
            index
            element={
              <>
                <Hero />
                <Thegame />
                <Features />
                <Sysreq />
                <Topscore />
                <Subscribe />
                <Footer />
              </>
            }
          />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/media" element={<Media />} />
          <Route path="/gamelist" element={<Gamelist />} />
          <Route path="/gamelist/:id" element={<GameDetail />} />
          <Route path="/users" element={<UserList />} />
          <Route path="/users/:id" element={<UserDetail />} />
          <Route path="/users/edit/:id" element={<EditUser />} />
        </Route>
        <Route path="/gamesuite" element={<GameSuite />} />
        <Route path="/tetris" element={<Tetris />} />
        <Route path="/snake" element={<Snake />} />
      </>
    )
  )
  return <RouterProvider router={router} />
}

export default App
