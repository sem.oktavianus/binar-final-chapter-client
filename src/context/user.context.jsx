import React, { createContext, useState } from 'react'

const userData = JSON.parse(localStorage.getItem('userData'))

export const UserContext = createContext({
  setCurrentUser: () => userData,
  currentUser: userData
})

// eslint-disable-next-line react/prop-types
const UserProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(userData)
  const value = { currentUser, setCurrentUser }
  return <UserContext.Provider value={value}>{children}</UserContext.Provider>
}

export default UserProvider
