import "../../../node_modules/video-react/dist/video-react.css"
import React from "react";
import { Player, ControlBar } from "video-react";

const sources = {  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
test: 'http://media.w3.org/2010/05/video/movie_300.webm'
}

class Video extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            source: sources.sintelTrailer
        };

        this.play = this.play.bind(this);
        this.pause = this.pause.bind(this);
        this.load = this.load.bind(this);
        this.changeCurrentTime = this.changeCurrentTime.bind(this);
        this.seek = this.seek.bind(this);
        this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this);
        this.changeVolume = this.changeVolume.bind(this);
        this.setMuted = this.setMuted.bind(this);

    }

    componentDidMount() {
        this.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    setMuted(muted) {
        return () => {
            this.player.muted = muted;
        };
    }

    handleStateChange(state) {
        this.setState({ player: state });
    }

    play() {

        this.player.play();

    }
    pause() {
        this.player.pause();
    }

    load() { this.player.load(); }

    changeCurrentTime(seconds) {
        return () => {
            const { player } = this.player.getState();
            this.player.seek(player.currentTime + seconds);
        };

    }

    seek(seconds) {
        return () => {
            this.player.seek(seconds);
        };
    }

    changePlaybackRateRate(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.playbackRate = player.playbackRate + steps;
        };
    }

    changeVolume(steps) {
        return () => {
            const { player } = this.player.getState();
            this.player.volume = player.volume + steps;
        };
    }

    changeSource(name) {
        return () => {
            this.setState({
                source: sources[name]
            });
            this.player.load();
        };
    }

    render(){
        return (
            <div >

                <h2 className="text-white-50">
                    reference : <a href='https://github.com/video-react/video-react'>https://github.com/video-react/video-react</a>
                </h2>
                <Player
                    playsInline
                    ref={
                        player => {
                            this.player = player;
                        }
                    }

                    autoPlay>
                    <source src={this.state.source} />
                    <ControlBar autoHide={false} />

                </Player>
                
                <div className="d-flex flex-row justify-content-between p-4">
                    <button onClick={this.play} className='p-3 btn btn-secondary '>
                        Play
                    </button>
                    <button onClick={this.pause} className='p-3 btn btn-warning'>
                        Pause
                    </button>
                    <button onClick={this.load} className='p-3 btn btn-secondary'>
                        Load
                    </button>
                </div> 

                <div className="d-flex flex-row justify-content-between p-2">

                    <button onClick={this.changePlaybackRateRate(1)} className="p-4 btn btn-warning ">
                        playbackRate++
                    </button>

                    <button onClick={this.changePlaybackRateRate(-1)} className="p-4 btn btn-secondary "> playbackRate-
                    </button>

                    <button onClick={this.changePlaybackRateRate(0.1)} className="p-4 btn btn-secondary ">
                        playbackRate+=0.1
                    </button>

                    <button onClick={this.changePlaybackRateRate(-0.1)} className="p-4 btn btn-warning ">
                        playbackRate-=0.1
                    </button>

                </div>

                <div className="d-flex flex-row justify-content-between p-2">
                    <button onClick={this.changeVolume(0.1)} className="p-4 btn btn-secondary">
                        volume+=0.1
                    </button>

                    <button onClick={this.changeVolume(-0.1)} className="p-4 btn btn-warning">
                        volume-=0.1
                    </button>

                    <button onClick={this.setMuted(true)} className="p-4 btn btn-warning">
                        muted=true
                    </button>

                    <button onClick={this.setMuted(false)} className="p-4 btn btn-secondary">
                        muted=false
                    </button>

                </div>

                <h1 className="text-white p-5">Playlist Video</h1>

                <div className="d-flex flex-row justify-content-between p-1">

                    <button onClick={this.changeSource('sintelTrailer')} className="p-2 btn btn-warning">
                        Sintel teaser
                    </button>

                    <button onClick={this.changeSource('bunnyTrailer')} className="p-2 btn btn-secondary">
                        Bunny trailer
                    </button>

                    <button onClick={this.changeSource('test')} className="p-2 btn btn-warning">
                        Test movie
                    </button>
                </div>         
                
            </div >
        )
    }
}
export default Video;