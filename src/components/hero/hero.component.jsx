import React from 'react'

const Hero = () => {
  return (
		<>
			<div className="hero" id="header">
				<div className="container d-flex flex-column justify-content-center align-items-center">
					<h1 className="h1 mt-5 mb-5">Play traditional game</h1>
					<p className="sub-text mb-5">Experience new traditional game play</p>
					<button
						onClick={() => (window.location.href = '/gamesuite')}
						type="button"
						className="btn btn-warning text-uppercase fw-bold button-main"
					>
						play now
					</button>
				</div>
				<div className="bottom-link d-flex flex-column align-items-center">
					<p className="text-white text-uppercase">The Game</p>
					<a href="#the-game">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="16"
							height="16"
							fill="currentColor"
							className="bi bi-caret-down"
							viewBox="0 0 16 16"
						>
							<path d="M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z" />
						</svg>
					</a>
				</div>
			</div>
		</>
  )
}

export default Hero
