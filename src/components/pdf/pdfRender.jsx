import React from 'react'
import { PDFViewer } from "@react-pdf/renderer/lib/react-pdf.browser.cjs.js";
import { Component } from "react";
import MyDocument from "../dokumen/dokumen";

export default class PdfRender extends Component {
    render() {
        return (
                <PDFViewer width={'100%'} height={625}>
                  <MyDocument />
              </PDFViewer>                
        )
    }
}