import React from 'react'
import { PDFDownloadLink } from "@react-pdf/renderer/lib/react-pdf.browser.cjs.js";
// import { Button } from "flowbite-react";
import { Component } from "react";
import MyDocument from "../dokumen/dokumen";

export default class PdfDownload extends Component {
    render() {
        return (
              <button className="btn btn-primary p-2">
                <PDFDownloadLink document={<MyDocument />} fileName="Untitled.pdf">
                  {({ loading }) =>
                    loading ? 'Loading document...' : 'Download now!'
                  }
                </PDFDownloadLink>
            </button>                 
        )
    }
}