import React from 'react'

const Features = () => {
  return (
		<>
			<section className="features d-flex align-items-center" id="features">
				<div className="container">
					<div className="row">
						<div className="col-lg-5"></div>
						<div className="col-lg-2"></div>
						<div className="col-lg-5">
							<div className="mb-3 ms-3">
								<p className="sub-text mb-0">What&aposs so Special?</p>
								<h2 className="h2">features</h2>
								<div className="steps" id="accordionExample">
									<div className="accordion-item active-steps">
										<div className="steps-item">
											<button
												className="h3 steps-button"
												type="button"
												data-bs-toggle="collapse"
												data-bs-target="#collapseOne"
												aria-expanded="true"
												aria-controls="collapseOne"
											>
												<span className="steps-dot"></span>traditional game
											</button>
										</div>
										<div
											id="collapseOne"
											className="accordion-collapse collapse show"
											aria-labelledby="headingOne"
											data-bs-parent="#accordionExample"
										>
											<div className="steps-body body-sentence">
												if you miss yout childhood, we provide many traditional
												games here
											</div>
										</div>
									</div>

									<div className="accordion-item">
										<div className="steps-item">
											<button
												className="h3 steps-button"
												type="button"
												data-bs-toggle="collapse"
												data-bs-target="#collapseTwo"
												aria-expanded="true"
												aria-controls="collapseTwo"
											>
												<span className="steps-dot"></span>game suit
											</button>
										</div>

										<div
											id="collapseTwo"
											className="accordion-collapse collapse"
											aria-labelledby="headingTwo"
											data-bs-parent="#accordionExample"
										>
											<div className="steps-body body-sentence">
												Lorem ipsum, dolor sit amet consectetur adipisicing
												elit. Fugiat, repellendus!
											</div>
										</div>
									</div>
									<div className="accordion-item">
										<div className="steps-item">
											<button
												className="h3 steps-button"
												type="button"
												data-bs-toggle="collapse"
												data-bs-target="#collapseThree"
												aria-expanded="true"
												aria-controls="collapseThree"
											>
												<span className="steps-dot"></span>tbd
											</button>
										</div>
										<div
											id="collapseThree"
											className="accordion-collapse collapse"
											aria-labelledby="headingThree"
											data-bs-parent="#accordionExample"
										>
											<div className="steps-body body-sentence">
												Lorem ipsum dolor sit amet consectetur, adipisicing
												elit. Nobis, exercitationem?
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</>
  )
}

export default Features
