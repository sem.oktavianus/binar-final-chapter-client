import React from 'react'
// import { Button } from "flowbite-react";
import { useEffect, useRef } from "react";
const UploadWidget = () => {
    const cloudinaryRef = useRef();
    const widgetRef = useRef();
    useEffect (() => {
        cloudinaryRef.current = window.cloudinary;
        widgetRef.current = cloudinaryRef.current.createUploadWidget({
            cloudName: 'ddlmgwsnb',
            uploadPreset: 'jhuisiyy'
        }, function() {
            // console.log(result);
        });
    }, [])

    return (
        <>
        <h3 className="text-white">UPLOAD FILE DISINI</h3>
        <button className="btn btn-primary p-2" onClick={() => widgetRef.current.open()}>
            Upload File
        </button>
        </>
        
    )
}
export default UploadWidget;