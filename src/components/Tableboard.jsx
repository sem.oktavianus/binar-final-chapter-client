/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import config from "../config";

const Tableboard = () => {
	const [users, setUser] = useState([]);

	useEffect(() => {
		getUsers();
	}, []);

	const getUsers = async () => {
		const response = await axios.get(`${config.baseUrl}}/users`);
		setUser(response.data);
	};

	return (
		<div>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>#</th>
						<th>Username</th>
						<th>Point</th>
					</tr>
				</thead>
				<tbody>
					{users.map((user, index) => (
						<tr key={user.id}>
							<td>{user.index + 1}</td>
							<td>{user.username}</td>
							<td>{user.point}</td>
						</tr>
						
					))}
				</tbody>
			</Table>
		</div>
	);
};

export default Tableboard;
