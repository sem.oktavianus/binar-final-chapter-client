import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import React from 'react'

const image = (props) => { const { image } = props; return image }

const Cardgame = (props) => {
  return (
		<div>
			<Card style={{ width: '18rem' }}>
				<Card.Img variant='top' src={image(props)} />
				<Card.Body>
					<Card.Title>Card Title</Card.Title>
					<Card.Text>
						Some quick example text to build on the card title and make up the
						bulk of the card&aposs content.
					</Card.Text>
					<Button variant='dark'>Play Now</Button>
				</Card.Body>
			</Card>
		</div>
  )
}

export default Cardgame
