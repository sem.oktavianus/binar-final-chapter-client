import React, { useEffect, useReducer } from 'react'
import { useParams, Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { selectCurrentUser } from '../../store/user/user.selector'
import axios from 'axios'
import config from '../../config'

const gameReducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_GAME_SUCCESS':
      return { ...state, game: action.payload, isLoading: false }
    case 'FETCH_GAME_FAILURE':
      return { ...state, error: action.payload, isLoading: false }
    default:
      return state
  }
}

const GameDetail = () => {
  const currentUser = useSelector(selectCurrentUser)
  const [state, dispatch] = useReducer(gameReducer, {
    game: {},
    isLoading: true,
    error: null
  })
  const { id } = useParams()
  const { game, isLoading, error } = state

  const updatePlayers = async () => {
    const newPlayers = [...game.data.players, currentUser.id]
    try {
      await axios.put(`${config.baseUrl}/game/update/${id}`, {
        ...game.data,
        players: newPlayers
      })
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    axios
      .get(`${config.baseUrl}/game/${id}`)
      .then((res) => {
        dispatch({ type: 'FETCH_GAME_SUCCESS', payload: res.data })
      })
      .catch((error) => {
        dispatch({ type: 'FETCH_GAME_FAILURE', payload: error.message })
      })
  }, [id])

  if (isLoading) {
    return <div> Loading...</div>
  }

  if (error) {
    return <div>Error: {error}</div>
  }

  return (
		<section
			className='d-flex flex-column justify-content-center align-items-center'
			id='top-scores'
		>
			<div className='container'>
				<div
					className='card text-white card-has-bg click-col m-auto mt-5'
					style={{ backgroundImage: '', width: '75%', textAlign: 'center' }}
				>
					<table
						className='table detail'
						style={{ color: '#fff', fontSize: '2rem', lineHeight: '1.5' }}
					>
						<tbody>
							<tr>
								<td>Title</td>
								<td> : {game.data.title}</td>
							</tr>
							<tr>
								<td>Description</td>
								<td> : {game.data.description}</td>
							</tr>
							<tr>
								<td>How to Play</td>
								<td> : {game.data.howToPlay}</td>
							</tr>
						</tbody>
					</table>
					{ currentUser
					  ? (
					  	<Link to={game.data.link}>
							<button
								onClick={updatePlayers}
								type='submit'
								className='btn btn-warning fw-bold button-main mt-5'
							>
								Play Game
							</button>
						</Link>
					    )
					  : null}
				</div>
			</div>
		</section>
  )
}

export default GameDetail
