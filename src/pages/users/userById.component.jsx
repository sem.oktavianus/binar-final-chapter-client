import React, { useEffect, useState } from 'react'
// import { UserContext } from "../../context/user.context";
import { useSelector } from 'react-redux'
import { selectCurrentUser } from '../../store/user/user.selector'
import { useParams, Link } from 'react-router-dom'
import axios from 'axios'
import config from '../../config'

function UserDetail () {
  // const { currentUser, setCurrentUser } = useContext(UserContext);
  const currentUser = useSelector(selectCurrentUser)
  const [user, setUser] = useState({})
  const { id } = useParams()
  useEffect(() => {
    axios.get(`${config.baseUrl}/users/${id}`, {}).then((res) => {
      setUser(res.data.data)
    })
  }, [id])

  return (
		<>
			<div className="users">
				<div className="h2 mb-4">User Detail</div>
				<div className="box">
					<table className="table detail">
						<tbody>
							<tr>
								<td>Nama</td>
								<td>{user.name}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{user.email}</td>
							</tr>
							<tr>
								<td>City</td>
								<td>{user.city || ''}</td>
							</tr>
							<tr>
								<td>Biodata</td>
								<td>{user.biodata || ''}</td>
							</tr>
							<tr>
								<td>Total Score</td>
								<td>{user.score || 0}</td>
							</tr>
							<tr>
								<td>social media</td>
								<td>{user.socialMedia || ''}</td>
							</tr>
						</tbody>
					</table>

					{currentUser?.email === user.email
					  ? (
						<Link to={`/users/edit/${currentUser.id}`}>
							<button
								type="submit"
								className="btn btn-warning fw-bold button-main mt-5"
							>
								Edit My Profile
							</button>
						</Link>
					    )
					  : null}
				</div>
			</div>
		</>
  )
}

export default UserDetail
