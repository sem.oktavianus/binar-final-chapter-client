// import {
// 	auth,
// 	signInWithGooglePopup,
// 	signInWithGoogleRedirect,
// } from "../../libs/firebase/firebase.libs";
import React, { useEffect, useState } from 'react'
// import { UserContext } from "../../context/user.context";
import { useDispatch, useSelector } from 'react-redux'
import {
  setCurrentUser,
  setCurrentUserWithGoogle
} from '../../store/user/user.action'
import {
  selectCurrentUser,
  selectIsLoading,
  selectError
} from '../../store/user/user.selector'
import { FcGoogle } from 'react-icons/fc'
import { useNavigate } from 'react-router-dom'
// import axios from "axios";
// import { setAuthToken } from "../../SetAuthToken";
// import config from "../../config";
function Login () {
  const isLoading = useSelector(selectIsLoading)
  const error = useSelector(selectError)
  const dispatch = useDispatch()
  const currentUser = useSelector(selectCurrentUser)
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  // const { currentUser, setCurrentUser } = useContext(UserContext);
  const navigate = useNavigate()
  const logGoogleUser = () => {
    dispatch(setCurrentUserWithGoogle())
    // const { user } = await signInWithGooglePopup();
    // if (user) {
    // 	axios.get(`${config.baseUrl}/users/email/${user.email}`).then((res) => {
    // 		if (!res.data) {
    // 			axios
    // 				.post(`${config.baseUrl}/users/registergoogle`, {
    // 					email: user.email,
    // 					name: user.displayName,
    // 					id: user.uid,
    // 				})
    // 				.then((res) => {
    // 					dispatch(setCurrentUser({ ...res.data }));
    // 					// setCurrentUser({
    // 					// 	...res.data.data,
    // 					// });
    // 					// localStorage.setItem(
    // 					// 	"userData",
    // 					// 	JSON.stringify({
    // 					// 		...res.data.data,
    // 					// 	})
    // 					// );
    // 				});
    // 		} else {
    // 			dispatch(setCurrentUser({ ...res.data }));
    // 			// setCurrentUser({
    // 			// 	...res.data,
    // 			// });
    // 			// localStorage.setItem(
    // 			// 	"userData",
    // 			// 	JSON.stringify({
    // 			// 		...res.data,
    // 			// 	})
    // 			// );
    // 		}
    // 	});
    // }
    currentUser && navigate('/')
  }
  const loginHandler = (e) => {
    e.preventDefault()

    dispatch(setCurrentUser(email, password))
    // localStorage.setItem("userData", JSON.stringify(datauser));
    // setCurrentUser(datauser);
    // setAuthToken(token);

    currentUser && navigate('/')
  }

  useEffect(() => {
    if (currentUser) {
      navigate('/')
    }
  }, [currentUser])
  return (
		<>
			<div className="container">
				<div className="d-flex justify-content-center mt-2">
					<div className="card custom-card">
						<div className="card-header">
							<h2 className="h2 text-center">Sign In</h2>
						</div>
						<div className="card-body">
							<form method="post">
								<div className="d-flex flex-column gap-3">
									<div className="input-cover">
										<input
											type="email"
											placeholder="youremail@binar.co.id"
											id="username"
											name="email"
											value={email}
											onChange={(e) => {
											  setEmail(e.target.value)
											}}
										/>
										<label htmlFor="username">Username</label>
									</div>
									<div className="input-cover">
										<input
											type="password"
											placeholder="********"
											id="password"
											name="password"
											value={password}
											onChange={(e) => {
											  setPassword(e.target.value)
											}}
										/>
										<label htmlFor="password">Password</label>
									</div>
									<button
										onClick={loginHandler}
										type="submit"
										className="btn btn-warning fw-bold button-main"
									>
										{isLoading ? 'LOADING...' : 'LOG IN'}
									</button>
								</div>
							</form>
						</div>
						<div className="card-footer">
							<div className="d-flex justify-content-center links fs-3 mb-3 text-light">
								<p>
									Don&apost have an account? <a href="/register"> Register</a>
								</p>
							</div>

							<p className="fs-3 text-light text-center">
								Or Sign In with{' '}
								<FcGoogle
									onClick={logGoogleUser}
									style={{ fontSize: '40px', cursor: 'pointer' }}
								/>
							</p>
						</div>
						{error && (
							<div
								className="alert alert-warning alert-dismissible fade show mt-3"
								role="alert"
							>
								<strong>Login Failed!</strong> Please try again
								<button
									type="button"
									className="btn-close"
									data-bs-dismiss="alert"
									aria-label="Close"
								></button>
							</div>
						)}
					</div>
				</div>
			</div>
		</>
  )
}

export default Login
