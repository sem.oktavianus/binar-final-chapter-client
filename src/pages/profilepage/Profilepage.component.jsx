import React from 'react'
import Tableboard from '../../components/Tableboard'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

const Profilepage = () => {
  return (
    <div>
        <div className=" container row">
            <div className="col-6 my-5">
                <h3>Profile Player</h3>
                <Form>
                    <Form.Group className="mb-3 text-start">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Input Your Name" />
                    </Form.Group>
                    <Form.Group className="mb-3 text-start">
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control type="text" placeholder="Input Your Phone Number" />
                    </Form.Group>
                    <Form.Group className="mb-3 text-start">
                        <Form.Label>Job</Form.Label>
                        <Form.Control type="text" placeholder="Input Your Job" />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Edit
                    </Button>
                </Form>
            </div>
            <div className="col-6 text-center my-5">
                <h3 className='mb-4'>All Players</h3>
                <Tableboard/>
            </div>
        </div>
    </div>
  )
}

export default Profilepage
