import React from 'react'
import PdfDownload from '../../components/pdf/pdfDownload'
import PdfRender from '../../components/pdf/pdfRender'
import Video from '../../components/video/video'
import UploadWidget from '../../components/upload/upload'

function Media() {
  return (
    <div className="media">
      <div className="h2 mb-4">Media Handling</div>
      <div className="box1">
        <Video />
      </div>

      <div className="box2">
        <PdfRender />
        <PdfDownload />
      </div>

      <div className="box3">
        <UploadWidget />
      </div>
    </div>
  )
}
export default Media
