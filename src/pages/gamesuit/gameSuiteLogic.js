export const randomChoice = () => {
  const choices = ['batu', 'kertas', 'gunting']
  const random = Math.floor(Math.random() * choices.length)
  return choices[random]
}

export const getResult = (player1, player2) => {
  switch (player1 + player2) {
    case 'batubatu':
      return 'draw'
    case 'batukertas':
      return 'com win'
    case 'batugunting':
      return 'player 1 win'
    case 'kertasbatu':
      return 'player 1 win'
    case 'kertaskertas':
      return 'draw'
    case 'kertasgunting':
      return 'com win'
    case 'guntingbatu':
      return 'com win'
    case 'guntingkertas':
      return 'player 1 win'
    case 'guntinggunting':
      return 'draw'
    default:
  }
}
export const INITIAL_STATE = {
  playerChoice: null,
  comChoice: null,
  result: null,
  refresh: false,
  playSound: null,
  score: 0
}

export const GameSuiteReducer = (state, action) => {
  const { type, payload } = action
  switch (type) {
    case 'PLAYER.BATU':
      return {
        ...state,
        playerChoice: payload.playerOne,
        comChoice: payload.com,
        result: getResult(payload.playerOne, payload.com),
        score:
					getResult(payload.playerOne, payload.com) === 'player 1 win'
					  ? state.score + 10
					  : state.score
      }
    case 'PLAYER.GUNTING':
      return {
        ...state,
        playerChoice: payload.playerOne,
        comChoice: payload.com,
        result: getResult(payload.playerOne, payload.com),
        score:
					getResult(payload.playerOne, payload.com) === 'player 1 win'
					  ? state.score + 10
					  : state.score
      }
    case 'PLAYER.KERTAS':
      return {
        ...state,
        playerChoice: 'kertas',
        comChoice: payload.com,
        result: getResult(payload.playerOne, payload.com),
        score:
					getResult(payload.playerOne, payload.com) === 'player 1 win'
					  ? state.score + 10
					  : state.score
      }
    case 'REFRESH':
      return {
        ...state,
        playerChoice: null,
        comChoice: null,
        result: null,
        refresh: false,
        score: 0
      }
  }
}
