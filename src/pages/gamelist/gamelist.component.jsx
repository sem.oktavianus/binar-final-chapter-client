import axios from 'axios'
import React, { useEffect, useState } from 'react'
import config from '../../config'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { selectCurrentUser } from '../../store/user/user.selector'

function Gamelist () {
  const [games, setGames] = useState([])
  const currentUser = useSelector(selectCurrentUser)

  useEffect(() => {
    axios
      .get(`${config.baseUrl}/game`)
      .then((response) => {
        // Use response.data instead of just response
        setGames(response.data)
      })
      .catch((error) => {
        console.log('Error:', error)
      })
  }, []) // Add an empty dependency array to avoid infinite useEffect loop

  return (
		<>
			<section className='wrapper'>
				<div className='container'>
					<div className='row row-cols-1 row-cols-md-2 row-cols-lg-4'>
						{games.map((game) => {
						  // Remove unused key prop & index parameter
						  return (
								<div className='col mb-3' key={game.id}>
									<Link to={`${game.id}`}>
										<div
											className='card text-white card-has-bg click-col'
											style={{
											  backgroundColor: 'rgba(0,0,0,0.9)',
											  backgroundImage: `url(${game.data.imageUrl})`
											}} // Use url() to set background image
										>
											<img
												className='card-img d-none'
												src=''
												alt='Government Lorem Ipsum Sit Amet Consectetur dipisi?'
											/>
											<div
												className='card-img-overlay d-flex flex-column'
												style={{
												  backgroundColor: 'rgba(0,0,0,0.65)'
												}}
											>
												<div className='card-body'>
													<h5 className='card-meta mb-2'>Available</h5>
													<h2 className='card-title mt-0 '>
														<span className='text-white'>
															{game.data.title}
														</span>
													</h2>
												</div>
												<div className='card-footer'>
													<div className='media'>{game.data.description}</div>
													<h4 className='text-warning mt-2'>
														{game.data.players?.includes(currentUser.id) &&
															'You already play this game'}
														{/* Use && operator to conditionally render the message */}
													</h4>
												</div>
											</div>
										</div>
									</Link>
								</div>
						  )
						})}
					</div>
				</div>
			</section>
		</>
  )
}

export default Gamelist
