import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import config from '../../config'
function Register () {
  const navigate = useNavigate()
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const registerHandler = async (e) => {
    e.preventDefault()
    try {
      await axios.post(`${config.baseUrl}/users/register`, {
        name,
        email,
        password
      })
      navigate('/login')
    } catch (error) {
      console.error(error)
    }
  }
  return (
		<>
			<div className="container">
				<div className="d-flex justify-content-center mt-2">
					<div className="card">
						<div className="card-header">
							<h2 className="h2 text-center">Sign Up</h2>
						</div>
						<div className="card-body">
							<form action="" method="post">
								<div className="d-flex flex-column gap-3">
									<div className="input-cover">
										<input
											onChange={(e) => {
											  setName(e.target.value)
											}}
											type="text"
											placeholder="full name"
											id="fullname"
											name="name"
											value={name}
										/>
										<label htmlFor="fullname">Full Name</label>
									</div>
									<div className="input-cover">
										<input
											onChange={(e) => {
											  setEmail(e.target.value)
											}}
											type="email"
											placeholder="youremail@binar.co.id"
											id="username"
											name="email"
											value={email}
										/>
										<label htmlFor="username">Username</label>
									</div>
									<div className="input-cover">
										<input
											type="password"
											placeholder="********"
											id="password"
											name="password"
											value={password}
											onChange={(e) => {
											  setPassword(e.target.value)
											}}
										/>
										<label htmlFor="password">Password</label>
									</div>
									<button
										onClick={registerHandler}
										type="submit"
										className="btn btn-warning fw-bold button-main"
									>
										REGISTER
									</button>
								</div>
							</form>
						</div>
						<div className="card-footer">
							<div className="d-flex justify-content-center links fs-3 text-light">
								<p>
									Have an account? <a href="/login"> Log In</a>
								</p>
							</div>
							<p className="fs-3 text-light text-center">
								Back to <a href="/">Home</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</>
  )
}

export default Register
