import React from 'react'
import { useSelector } from 'react-redux'
import { selectCurrentUser } from '../../store/user/user.selector'
import Login from '../login/login.component'

const Snake = () => {
  const currentUser = useSelector(selectCurrentUser)
  return currentUser
    ? (
		<h1 className="text-white display-1">SNAKE GAME</h1>
      )
    : (
		<Login />
      )
}

export default Snake
