const USER_ACTION_TYPES = {
  SET_CURRENT_USER: 'user/SET_CURRENT_USER',
  SET_USER_SCORE: 'user/SET_USER_SCORE',
  SET_USER_LOGOUT: 'user/SET_USER_LOGOUT',
  SET_FETCH_USER_START: 'user/SET_FETCH_USER_START',
  SET_FETCH_USER_SUCCESS: 'user/SET_FETCH_USER_SUCCESS',
  SET_FETCH_USER_ERROR: 'user/SET_FETCH_USER_ERROR'
}
export default USER_ACTION_TYPES
