import USER_ACTION_TYPES from './user.types'
import { createAction } from '../../libs/reducer/reducers.util'
import axios from 'axios'
import config from '../../config'
import { signInWithGooglePopup } from '../../libs/firebase/firebase.libs'

export const fetchUserStart = () =>
  createAction(USER_ACTION_TYPES.SET_FETCH_USER_START)
export const fetchUserSuccess = (user) =>
  createAction(USER_ACTION_TYPES.SET_FETCH_USER_SUCCESS, user)
export const fetchUserError = (error) =>
  createAction(USER_ACTION_TYPES.SET_FETCH_USER_ERROR, error)
export const setCurrentUser = (email, password) => {
  return (dispatch) => {
    dispatch(fetchUserStart())

    setTimeout(async () => {
      try {
        const user = await axios.post(`${config.baseUrl}/users/login`, {
          email,
          password
        })
        const { ...datauser } = { ...user.data }

        dispatch(fetchUserSuccess(datauser))
      } catch (error) {
        dispatch(fetchUserError(error))
      }
    }, 1000)
  }
}

export const setCurrentUserWithGoogle = () => {
  return async (dispatch) => {
    dispatch(fetchUserStart())
    try {
      const { user } = await signInWithGooglePopup()
      if (user) {
        axios.get(`${config.baseUrl}/users/email/${user.email}`).then((res) => {
          if (!res.data) {
            axios
              .post(`${config.baseUrl}/users/registergoogle`, {
                email: user.email,
                name: user.displayName,
                id: user.uid
              })
              .then((res) => {
                dispatch(fetchUserSuccess({ ...res.data }))
              })
          } else {
            dispatch(fetchUserSuccess({ ...res.data }))
          }
        })
      }
    } catch (error) {
      dispatch(fetchUserError(error))
    }
  }
}

export const setUserScore = (currentUser, score) => {
  return (dispatch) => {
    dispatch(fetchUserStart())
    try {
      setTimeout(async () => {
        const update = await axios.put(
					`${config.baseUrl}/users/update/${currentUser.id}`,
					{
					  ...currentUser,
					  score
					}
        )
        console.log(update.data.data)
        dispatch(fetchUserSuccess(update.data.data))
      }, 1000)
    } catch (error) {
      dispatch(fetchUserError(error))
    }
  }
}

export const setUserUpdate = (userUpdate) => {
  return (dispatch) => {
    dispatch(fetchUserStart())
    try {
      setTimeout(async () => {
        const update = await axios.put(
					`${config.baseUrl}/users/update/${userUpdate.id}`,
					{
					  ...userUpdate
					}
        )
        dispatch(fetchUserSuccess(update.data.data))
      }, 1000)
    } catch (error) {
      dispatch(fetchUserError(error))
    }
  }
}
export const setUserLogout = () =>
  createAction(USER_ACTION_TYPES.SET_USER_LOGOUT)
