export const selectCurrentUser = (state) => state.user.currentUser
export const selectIsLoading = (state) => state.user.isLoading
export const selectUserScore = (state) => state.user.currentUser.score
export const selectError = (state) => state.user.error
